var express = require("express");
var path = require("path");
const cors = require("cors");
const compress = require("compression");
const helmet = require('helmet');
var app = express();
app.use(compress());
app.use(helmet());
app.use(cors())
app.use(express.static(path.join(__dirname, "dist/pulsesurvey")));

app.use(express.static("dist/pulsesurvey", { index: "/index.html" }));

app.listen(3000 , () =>console.log('connected'));


app.use(function(req, res, next) {
  if (/^\/v1.0\//.test(req.url)) {
    next();
  } else {
    res.sendFile(__dirname + "/dist/pulsesurvey/index.html");
  }
});
