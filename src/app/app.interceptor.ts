import { Injectable } from "@angular/core";
import { catchError } from "rxjs/operators";
import { HttpEvent, HttpRequest, HttpHandler, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AuthService } from "./auth.service";
import { config } from "./config";
import * as emailjs from "emailjs-com";
import * as jwt_decode from "jwt-decode";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  received_token: any;
  postDecode: any;
  constructor(public auth: AuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    console.log('testing');
    request = request.clone({
      setHeaders: {
        token: `${this.auth.getToken()}`,
        "Content-Type": "application/json",
      },
    });
    if (window.navigator.onLine) {
      //decoding token from localStorage
      const tokenInfo = this.getDecodedAccessToken(
        localStorage.getItem("token")
      );
      this.postDecode = JSON.stringify(tokenInfo);
      this.received_token = JSON.parse(this.postDecode);
      return next.handle(request).pipe(
        catchError((error, caught) => {
          //intercept the respons error and displace it to the console
          var host = window.location.host;
          if (this.received_token) {
            var templateParams = {
              msg: error.message,
              platform: host,
              user: this.received_token.email,
              role: this.received_token.role,
            };
            emailjs
              .send(
                "amazon_ses",
                config.emailTemplateID,
                templateParams,
                config.emailUserId
              )
              .then(
                (response) => {
                  console.log("SUCCESS!", response.status, response.text);
                  window.stop();
                },
                (err) => {
                  console.log("FAILED...", err);
                }
              );
          } else {
            var templateParams1 = {
              msg: error.message,
              platform: host,
              user: "unknown@example.com",
              role: "No Info",
            };
            emailjs
              .send(
                "amazon_ses",
                config.emailTemplateID,
                templateParams1,
                config.emailUserId
              )
              .then(
                (response) => {
                  console.log("SUCCESS!", response.status, response.text);
                  window.stop();
                },
                (err) => {
                  console.log("FAILED...", err);
                }
              );
          }
          // alert("Something went wrong, Please try after sometime!");

          // this.handleAuthError(error);
          return error;
        }) as any
      );
    } else {
      alert("Please Connect to internet");
    }
  }
  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }
}
