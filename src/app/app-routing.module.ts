import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { WelcomeComponent } from './welcome/welcome.component';
import { HomeComponent } from './components/home/home.component';
import {EmployeesComponent} from './components/employees/employees.component';


const routes: Routes = [
  { path: "", pathMatch: "full", component: WelcomeComponent, },
  { path: "admin", loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule) },
  { path: "survey", loadChildren: () => import('./survey/survey.module').then(m => m.SurveyModule) },
  { path: 'home', component: HomeComponent },
  { path: 'employees', component: EmployeesComponent}
  
];

@NgModule({
  imports: [
  RouterModule.forRoot(routes),
  HttpClientModule,
  BrowserAnimationsModule,
  FormsModule,
  ReactiveFormsModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
