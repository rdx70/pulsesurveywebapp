import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule, Routes } from "@angular/router";
import { ProfileComponent } from './profile/profile.component';
import { SubmitComponent } from './submit/submit.component';
import { HomeComponent } from './home/home.component';
const routes: Routes = [
  {
    path: "dashboard",
    component: HomeComponent,
    children: [
      {
        path: "form/:id",
        component: DashboardComponent,
      },
      {
        path: "edit-profile",
        component: ProfileComponent,
      },
      {
        path: "submit",
        component: SubmitComponent,
      }
    ]
  }
 

]

@NgModule({
  declarations: [DashboardComponent, ProfileComponent, SubmitComponent, HomeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class SurveyModule { }
