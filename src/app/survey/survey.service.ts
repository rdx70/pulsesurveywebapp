import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SurveyService {
    public pulseSurveyApiUrl = "/v1/pulsesurvey";
    public commonUserApiUrl = "/v1/user";
    public serverUrl = environment.apiURl;
    public surveyEngineApiUrl = '/v1/surveyEngineRequest';

    constructor(private http: HttpClient) {

    }

    roleGuard() {
        if (localStorage.getItem("role") != "Manager") {
            return false;
        } else {
            return true;
        }
    };

    getSurveyQuestions(data): Observable<any> {
      console.log(data);
      return this.http.get(this.serverUrl + this.commonUserApiUrl + "/getPulseSurveyTemplate", data)
            .pipe(map(response => {
                return response;
              // console.log(this.response,"wdadwd");
            }));
    }

    submitResponse(data): Observable<any> {
      console.log(data);
      return this.http.post(this.serverUrl + this.pulseSurveyApiUrl + "/submitResponse", data)
            .pipe(map(response => {
                return response;
              // console.log(this.response,"wdadwd");
            }));
    }

    getSingleParticipantDetails(data): Observable<any> {
      console.log(data);
      return this.http.post(this.serverUrl + this.commonUserApiUrl + "/getSingleParticipantDetails", {data: data})
            .pipe(map(response => {
                return response;
              // console.log(this.response,"wdadwd");
            }));
    }
}
