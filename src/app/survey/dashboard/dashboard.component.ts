import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SurveyService } from '../survey.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  surveyQuestions: any = [];
  surveyQuesitionOptions: any = [];
  surveyDescription = "";
  participantId: any;
  participantDetails: any;
  constructor(private surveyService: SurveyService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    // 5ea2e1438d981130ec0f206d
    this.participantId = { id: this.route.snapshot.params['id'] };
    console.log(this.participantId);
    this.surveyService.getSingleParticipantDetails(this.participantId.id).subscribe(response=>{
      if(response.status){
        console.log(response)
        this.participantDetails = response.data;
        console.log(this.participantDetails)
      }
    })
    this.fetchSurveyQuestions();
    this.fetchSurveyQuestions1();
    this.fetchSurveyQuestions2();
  }

  fetchSurveyQuestions() {
    let dataObject = {
      '_id': this.participantId
    }
    // console.log(dataObject,"second");
    this.surveyService.getSurveyQuestions(dataObject).subscribe(response => {
      console.log(response);
      if (response.status && typeof response.data !== 'undefined') {
        this.surveyQuestions = typeof response.data.questions !== 'undefined' ? response.data.questions : [];
        this.surveyQuesitionOptions = typeof response.data.selectOptions !== 'undefined' ? response.data.selectOptions : [];
        this.surveyDescription = typeof response.data.description !== 'undefined' ? response.data.description : "";
        // console.log(dataObject,"wdwadw");
      }
    })
  }
  fetchSurveyQuestions1() {
  let dataObject = {
    'templateName': 'Team Feedback Survey'
  }
  // console.log(dataObject,"second");
  this.surveyService.getSurveyQuestions(dataObject).subscribe(response => {
    console.log(response);
    if (response.status && typeof response.data !== 'undefined') {
      this.surveyQuestions = typeof response.data.questions !== 'undefined' ? response.data.questions : [];
      this.surveyQuesitionOptions = typeof response.data.selectOptions !== 'undefined' ? response.data.selectOptions : [];
      this.surveyDescription = typeof response.data.description !== 'undefined' ? response.data.description : "";
      // console.log(dataObject,"wdwadw");
    }
  })
}
fetchSurveyQuestions2() {
  let dataObject = {
    'templateName': 'Self Assessment'
  }
  // console.log(dataObject,"second");
  this.surveyService.getSurveyQuestions(dataObject).subscribe(response => {
    console.log(response);
    if (response.status && typeof response.data !== 'undefined') {
      this.surveyQuestions = typeof response.data.questions !== 'undefined' ? response.data.questions : [];
      this.surveyQuesitionOptions = typeof response.data.selectOptions !== 'undefined' ? response.data.selectOptions : [];
      this.surveyDescription = typeof response.data.description !== 'undefined' ? response.data.description : "";
      // console.log(dataObject,"wdwadw");
    }
  })
}
  captureResponse(questionId, answer) {
    let that = this;
    this.surveyQuestions.map(function (question, index) {
      if (question.id == questionId) {
        that.surveyQuestions[index].answer = answer;
      }
    });
    console.log('surveyQu', this.surveyQuestions);
  }

  isValid() {
    let isValid = true;
    let questionIds = [];

    this.surveyQuestions.map(function (value, index) {
      if (value.questionTypeId == '2' && (typeof value.answer === 'undefined' || value.answer == '' || value.answer == null)) {
        isValid = false;
        questionIds.push(value.id);
      }
    });

    return {
      'status': isValid,
      'questionIds': questionIds
    }
  }

  onSubmit(pageName) {
    let isValid = this.isValid();
    if (!isValid.status) {
      Swal.fire({
        icon: 'error',
        title: 'You have not answered the Questions ' + isValid.questionIds.join(",") + ' These questions are compulsory questions.',
        showConfirmButton: false,
        timer: 2500
      })
      // alert("Please fill compulsory questions");
      return false;
    }
    let data = {
      participantId: this.participantId,
      responses: this.surveyQuestions
    }
    this.surveyService.submitResponse(data).subscribe(response => {
      this.router.navigate(['/survey/dashboard/submit']);
    })
  }

  updateOnChangeResponse(questionId, evt) {
    let that = this;
    this.surveyQuestions.map(function (question, index) {
      if (question.id == questionId) {
        that.surveyQuestions[index].answer = evt.target.value;
      }
    });
  }
}
