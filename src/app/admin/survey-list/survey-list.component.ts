import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { AdminService } from '../admin.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
const ELEMENT_DATA: any = [];
import { AlertsService } from 'angular-alert-module';
import * as XLSX from "xlsx";
@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {

  // displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  displayedColumns = ['position','companyName', 'surveyName', 'startDate', 'endDate', 'responseRate', 'participant','surveyStatus', 'action', 'download'];
  dataSource: any = new MatTableDataSource(ELEMENT_DATA);
  surveyList = [];
  sheetData: any = [];
  finalData = [];
  isLoading =  false;
  today = new Date();
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private adminService: AdminService,
    private route: Router,
    private alerts: AlertsService) { }
  ngOnInit() {
    this.isLoading = true;
    this.adminService.getSurveyList().subscribe(response=>{
      console.log(response);
      if(response.status){
        response.data.forEach((value, index)=>{
          var surveyStatus;
          var currentDate = new Date(this.today),
              startDate = new Date(value.surveyStartDate),
              endDate = new Date(value.surveyEndDate),
              currentDateInSeconds = currentDate.getTime(),
              startDateInSeconds = startDate.getTime(),
              endDateInSeconds = endDate.getTime();

          if(currentDateInSeconds < endDateInSeconds && currentDateInSeconds > startDateInSeconds){
            surveyStatus = "Ongoing"
          } else if(currentDateInSeconds < startDateInSeconds){
            surveyStatus = "Upcoming"
          } else{
            surveyStatus = "Closed"
          }    
          let data = {
            position: index,
            companyName: value.companyName,
            surveyName: value.templateId.templateName,
            startDate: value.surveyStartDate,
            endDate: value.surveyEndDate,
            responseRate: response.responseRate[index],
            participant: response.respondents[index],
            surveyStatus: surveyStatus,
            _id: value._id
          }
          this.surveyList.push(data);
        })

        this.dataSource = new MatTableDataSource(this.surveyList);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.isLoading = false;
      }
    });

    
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  edit(id){
    this.route.navigate(['admin/dashboard/survey-configuration/'+id]);
  }
  download(id){
    this.isLoading = true;
    this.adminService.getParticipant({id: id}).subscribe(response=>{
      console.log(response);
      if(response.status){
        this.isLoading = false;
        response.data.forEach(value=>{
          if(value.surveySubmittedStatus == true){
            let eachData = {
            "Company Name": value.companyName,
            "Employee Name": value.employeeName,
            "Employee Email": value.employeeEmail,
            "My manager takes steps to ensure smooth business continuity.":value.surveyResponses[0].answer,
            "My manager cares about the overall wellbeing for me and my family.":value.surveyResponses[1].answer,
            "My manager addresses our concerns, fears and insecurities.":value.surveyResponses[2].answer,
            "My manager encourages us to learn new skills/ pursue learning.":value.surveyResponses[3].answer,
            "My manager comes up with interesting and novel ideas to connect with the team":value.surveyResponses[4].answer,
            "My manager ensures that I have the resources and equipment to do my job well":value.surveyResponses[5].answer,
            "My manager takes time out to personally connect with me":value.surveyResponses[6].answer,
            "My manager clearly communicates expectations from us":value.surveyResponses[7].answer,
            "My manager creates opportunities to celebrate and have fun together.":value.surveyResponses[8].answer,
            "My manager clearly communicates organizational plans to deal with the current situation.":value.surveyResponses[9].answer,
            "My manager is sensitive to the challenges I face while working remotely.":value.surveyResponses[10].answer,
            "My manager actively seeks suggestions and ideas from me to deal with the current situation.":value.surveyResponses[11].answer,
            "My manager helps me ensure that work does not spill over and take away personal time ":value.surveyResponses[12].answer,
            "Taking everything into account, my manager has dem…great people skills during the current situation.":value.surveyResponses[13].answer,
            "How would you rate your ability to be productive at work during the current situation?":value.surveyResponses[14].answer,
            "Rate your management’s readiness to ensure a smooth business transition post the lockdown.":value.surveyResponses[15].answer,
            "Mention 2-3 practices that your manager has undert…ent and positive energy in the team at this time?":value.surveyResponses[16].answer,
            "What are 1-2 areas where your manager could do mor…o drive more engagement in the team at this time?":value.surveyResponses[17].answer,
            "What is your age?":value.surveyResponses[18].answer,
            "What is your Gender?":value.surveyResponses[19].answer,
            "What is your level in your current organisation?":value.surveyResponses[20].answer,
            "What is your current industry?":value.surveyResponses[21].answer,
            "How long have you worked with your current Manager?":value.surveyResponses[22].answer,
            }
            this.finalData.push(eachData);
          }
        })
        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.finalData);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Sheet1");
        XLSX.writeFile(wb, "ManagerMIS.xlsx");
      } else{
        this.isLoading = false;
      }
    })
   }

}
