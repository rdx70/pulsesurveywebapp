import { environment } from "../../environments/environment";
import { Injectable } from "@angular/core";
// import { Headers } from "@angular/common/Headers";
// import { RequestOptions } from "@angular/RequestOptions";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from "@angular/router";
import { config } from "../config";
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  public managerApiUrl = "/v1/pulsesurvey";
  public commonUserApiUrl = "/v1/user";
  public serverUrl = environment.apiURl;
  public surveyEngineApiUrl = '/v1/surveyEngineRequest';

  constructor(private http: HttpClient) {  
  	
  }
  roleGuard() {
    if (localStorage.getItem("role") != "Manager") {
      return false;
    } else {
      return true;
    }
  };

  getSurveyTemplate(): Observable<any>{
  	 return this.http.get(this.serverUrl + this.managerApiUrl + "/getPulseSurveyTemplate")
     .pipe(map(response => {
      return response;
    }));
  }

  getAllCompanies(): Observable<any> {
    return this.http
      .get(this.serverUrl + this.surveyEngineApiUrl + '/allCompanies/')
      .pipe(map(response=>{
        return response;
      }));
  }

  getSingleCompany(id): Observable<any> {
    return this.http
      .post(this.serverUrl + this.surveyEngineApiUrl + '/singleCompany/', {id})
      .pipe(map(response=>{
        return response;
      }));
  }

  launchSurvey(data): Observable<any> {
    console.log(data);
    return this.http
      .post(this.serverUrl + this.managerApiUrl + '/launchSurvey', data)
      .pipe(map(response=>{
        return response;
      }));
  }

  getSurveyList(): Observable<any>{
     return this.http.get(this.serverUrl + this.managerApiUrl + "/getSurveyList")
     .pipe(map(response => {
      return response;
    }));
  }

  getParticipant(id): Observable<any>{
     return this.http.post(this.serverUrl + this.managerApiUrl + "/getParticipant", {id: id})
     .pipe(map(response => {
      return response;
    }));
  }

  updateParticipant(data): Observable<any>{
     return this.http.post(this.serverUrl + this.managerApiUrl + "/updateParticipant", data)
     .pipe(map(response => {
      return response;
    }));
  }

  deleteParticipant(id): Observable<any>{
     return this.http.post(this.serverUrl + this.managerApiUrl + "/deleteParticipant", {id: id})
     .pipe(map(response => {
      return response;
    }));
  }

  updateSurvey(data): Observable<any>{
     return this.http.post(this.serverUrl + this.managerApiUrl + "/updateSurvey", data)
     .pipe(map(response => {
      return response;
    }));
  }

  addParticipant(data): Observable<any>{
     return this.http.post(this.serverUrl + this.managerApiUrl + "/addParticipant", data)
     .pipe(map(response => {
      return response;
    }));
  }
  
  pulseSurveyAdminLogin(data): Observable<any>{
     return this.http.post(this.serverUrl + this.commonUserApiUrl + "/pulseSurveyAdminLogin", data)
     .pipe(map(response => {
      return response;
    }));
  }

  getGMCEmployees(data): Observable<any>{
     return this.http.post(this.serverUrl + this.managerApiUrl + "/getGMCEmployees", data)
     .pipe(map(response => {
      return response;
    }));
  }

}
