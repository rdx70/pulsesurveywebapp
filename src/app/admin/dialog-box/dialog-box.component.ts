import { Component, Inject, Optional } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
 
export interface UsersData {
  name: string;
  id: number;
}
 
 
@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.css']
})
export class DialogBoxComponent {
 
  action:string;
  local_data:any;
 
  constructor(
    public dialogRef: MatDialogRef<DialogBoxComponent>,
    //@Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data) {
    console.log(data);
    this.local_data = {...data};
    this.action = this.local_data.action;
  }
 
  doAction(){
  	console.log(this.local_data);
    this.dialogRef.close({event:this.action,data:this.local_data});
  }
  submitData(data){
    console.log(data);
  }
 
  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }
 
}