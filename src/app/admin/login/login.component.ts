import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../admin.service';
import Swal from 'sweetalert2';
import * as jwt_decode from "jwt-decode";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  postDecode: any;
  isLoading= false;
  received_token: any;
  public loginInvalid: boolean;
  private formSubmitAttempt: boolean;
  private returnUrl: string;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private adminService: AdminService
  ) {
  }

  async ngOnInit() {
    let tokenInfo = this.getDecodedAccessToken(localStorage.getItem("token"));
    this.postDecode = JSON.stringify(tokenInfo);
    this.received_token = JSON.parse(this.postDecode);
    if(tokenInfo != null){
      if(this.received_token.role == "Admin"){
        this.router.navigate(['/admin/dashboard']);
      }
    }
    this.form = this.fb.group({
      username: ['', Validators.email],
      password: ['', Validators.required]
    });

  }
  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  async onSubmit() {
    this.isLoading = true;
    this.loginInvalid = false;
    this.formSubmitAttempt = false;
    if (this.form.valid) {
      try {
        const username = this.form.get('username').value;
        const password = this.form.get('password').value;
        let data = {
        	email: username,
        	password: password
        }
        await this.adminService.pulseSurveyAdminLogin(data).subscribe(response=>{
        	console.log(response);
        	if(response.status){
            this.isLoading = false;
        		localStorage.setItem('token', response.userToken);
        		this.router.navigate(['admin/dashboard']);
            Swal.fire({
              icon: 'success',
              title: 'Login Successful',
              showConfirmButton: false,
              timer: 1500
            })
        	} else{
            this.isLoading = false;
            Swal.fire('Oops...', response.message, 'error')
          }
        })
      } catch (err) {
        this.loginInvalid = true;
      }
    } else {
      this.formSubmitAttempt = true;
    }
  }
}