import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { RangesFooter } from '../ranges-footer.component';
import { MatSort } from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../admin.service';
import { dateFormat, getCustomDate, dayAdd } from "../../config/utils";
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';
import { MatTable } from '@angular/material/table';
import * as XLSX from 'xlsx';
import Swal from 'sweetalert2';
import { NotifierService } from 'angular-notifier';
type AOA = any[][];
const ELEMENT_DATA: any = [];
@Component({
  selector: 'app-survey-management',
  templateUrl: './survey-management.component.html',
  styleUrls: ['./survey-management.component.css']
})
export class SurveyManagementComponent implements OnInit {
  private readonly notifier: NotifierService;
  today = dayAdd(Date.now(), "day", 1);
  maxDate = dayAdd(Date.now(), "day", 7);
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  isOptional = false;
  isLinear = false;
  getExelData = false;
  form: FormGroup;
  findSECompayId: Boolean;
  companyNameFromChild: string = '';
  newCompanyNameFromChild: string = '';
  inlineRange;
  excelSheetPlayGround: boolean = false;
  hrEmailSameWithManager: boolean = false;
  isExcelModalShown: boolean = false;
  excelData = [];
  excelType = false;
  newRespondants = [];
  companyID: any;
  respondants = [];
  data = [];
  touched = false;
  headers = [];
  errorArray = [];
  isLoading = false;
  headerAsObject = [];
  monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  mappingForm: FormGroup;
  templateId: any;
  wopts: XLSX.WritingOptions = {bookType: 'xlsx', type: 'array'};
  regExFormatEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  @ViewChild(MatTable,{static:true}) table: MatTable<any>;
  constructor(private _formBuilder: FormBuilder,
       private cd: ChangeDetectorRef,
       private route: ActivatedRoute,
       private router: Router,
       private adminService: AdminService,
       public dialog: MatDialog,
       notifierService: NotifierService) {
    this.notifier = notifierService;
  }

  
  displayedColumns: string[] = ['position', 'employeeName', 'employeeEmail', 'managerName', 'managerEmail', 'action'];
  dataSource: any = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  ngOnInit() {
    this.isLoading = true;
    this.dataSource = new MatTableDataSource([]);
    console.log(this.today);
    console.log(this.maxDate);
    this.templateId = { id: this.route.snapshot.params['id'] };
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required],
      date: [{begin: this.today, end: this.maxDate}]
    });
    this.secondFormGroup = this._formBuilder.group({
      file: [null, Validators.required]
    });
  }
  openDialog(action,obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data:obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.event == 'Add'){
        this.addRowData(result.data);
      }else if(result.event == 'Update'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData(result.data);
      }
    });
  }

  addRowData(row_obj){
    const found = this.dataSource.filteredData.some(el => el.employeeEmail === row_obj.employeeEmail);
    if (found){
      console.log(found)
      Swal.fire({
              icon: 'info',
              title: row_obj.employeeEmail+ ' is already in the list',
              showConfirmButton: true,
            })
    } else{
      var id = this.dataSource.filteredData.length;
      console.log(this.dataSource);
      this.dataSource.filteredData.push({
        id: id,
        employeeName:row_obj.employeeName,
        employeeEmail:row_obj.employeeEmail,
        managerName:row_obj.managerName,
        managerEmail:row_obj.managerEmail
      });
      console.log(this.dataSource);
      this.dataSource.filteredData.forEach((value, index)=>{
         value.id = index
      })
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.renderRows();
    }
    
    
  }
  updateRowData(row_obj){
    this.dataSource.filteredData = this.dataSource.filteredData.filter((value,key)=>{
      if(value.id == row_obj.id){
        value.employeeName = row_obj.employeeName;
        value.employeeEmail = row_obj.employeeEmail;
        value.managerName = row_obj.managerName;
        value.managerEmail = row_obj.managerEmail;
        let timerInterval
          Swal.fire({
            title: 'Hold on',
            html: 'Updating',
            timer: 500,
            timerProgressBar: true,
            onBeforeOpen: () => {
              Swal.showLoading()
              timerInterval = setInterval(() => {
                const content = Swal.getContent()
                if (content) {
                  var b = content.querySelector('b')
                  if (b) {
                    b.textContent = String(Swal.getTimerLeft())
                  }
                }
              }, 1000)
            },
            onClose: () => {
              clearInterval(timerInterval)
            }
          }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
              console.log('I was closed by the timer')
            }
          })
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
      }
      return true;
    });
  }
  deleteRowData(row_obj){
    console.log(row_obj);
    console.log(this.dataSource);
    // this.dataSource.filteredData = this.dataSource.filteredData.filter((value,key)=>{
    //   console.log(value.id);
    //   console.log(row_obj.id);
    //   return value.id != row_obj.id;
    // });
    var ind = this.dataSource.filteredData.findIndex(function(element){
       return element.id===row_obj.id;
    })
    if(ind!==-1){
    this.dataSource.filteredData.splice(ind, 1)
    }
    this.dataSource.filteredData.forEach((value, index)=>{
       value.id = index
    })
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.renderRows();
    console.log(this.dataSource);
  }
  inlineRangeChange($event) {
    this.inlineRange = $event;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  showExcelModal(): void {
    console.log("showing")
    this.isExcelModalShown = true;
  }
  getExtension(path) {
        var basename = path.split(/[\\/]/).pop(),  // extract file name from full path ...
            // (supports `\\` and `/` separators)
            pos = basename.lastIndexOf('.');       // get last position of `.`

        if (basename === '' || pos < 1)            // if file name is empty or ...
        {
            return '';
        }                             //  `.` not found (-1) or comes first (0)

        return basename.slice(pos + 1);            // extract extension ignoring `.`
    }
  onFileChange(evt: any) {
        this.touched = true;
        console.log(this.touched , 'touched')
        //console.log(evt.srcElement.files[0].name , 'fj');
        /* wire up file reader */
        let ext = this.getExtension(evt.srcElement.files[0].name);
        //console.log(ext)
        if (ext == 'csv' || ext == 'xls' || ext == 'xlsx') {
            //console.log('dd')
            this.excelType = true;
        } else {
            this.excelType = false;
        }
        const target: DataTransfer = <DataTransfer>evt.target;

        if (target.files.length !== 1) {
            throw new Error('Cannot use multiple files');
        }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            /* read workbook */
            const bstr: string = e.target.result;
            try {
                var wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});
                console.log('wb', wb);
            } catch (Error) {
                if (Error.name == 'Error') {
                    alert(Error.message);
                    (<HTMLInputElement>document.getElementById('fileSelect')).value = '';
                    return null;
                }
            }

            /* grab first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];

            /* save data */
            this.data = <AOA>XLSX.utils.sheet_to_json(ws, {header: 1});
            console.log('data', this.data);
            this.data =  this.data.filter(e => e.length);
            if (this.data[0] !== 'undefined') {
                let employeeNameHeader = this.data[0][0] !== 'undefined' ? this.data[0][0] : '';
                let employeeEmailHeader = this.data[0][1] !== 'undefined' ? this.data[0][1] : '';
                let managerNameHeader = this.data[0][2] !== 'undefined' ? this.data[0][2] : '';
                let manaerEmailHeader = this.data[0][3] !== 'undefined' ? this.data[0][3] : '';
                if (employeeNameHeader.toLowerCase() != 'employeename' 
                    && employeeEmailHeader.toLowerCase() != 'employeeemail' 
                    && managerNameHeader.toLowerCase() != 'managername' 
                    && manaerEmailHeader.toLowerCase() != 'manageremail') {
                  Swal.fire('Error Uploading Excel', 'Please check headers are correct or refer sample excel file', 'warning');
                    (<HTMLInputElement>document.getElementById('fileSelect')).value = '';
                    return;
                }
            }
            console.log(this.data);
            let modifiedData = [];
            let emailIds = [];  //Checking for duplicate
            let errorArray = [];
            let that = this;
            if (this.data !== undefined) {
                this.excelSheetPlayGround = true;
                this.data.map(function (value, index) {
                  console.log(value);
                  console.log(value[1]);
                    if(value.length >= 2){
                      if (index !== 0 && emailIds.indexOf(value[1]) == -1 && (value[0] != '' && value[1] != '' && typeof value[0] !== 'undefined' && typeof value[1] !== 'undefined')) {
                          modifiedData[index - 1] = {
                              'employeeName': '',
                              'employeeEmail': '',
                              'managerName': '',
                              'managerEmail': '',
                              'edit': false,
                              'employeeNameError': value[0] == '',
                              'managerNameError': value[2] == '',
                              'employeeEmailError': that.regExFormatEmail.test(value[1]) && value[1] != '' ? false : true,
                              'manageremailError': that.regExFormatEmail.test(value[3]) && value[3] != '' ? false : true
                          };
                        modifiedData[index - 1].employeeName = value[0];
                        modifiedData[index - 1].employeeEmail = value[1];
                        modifiedData[index - 1].managerName = value[2];
                        modifiedData[index - 1].managerEmail = value[3];
                        emailIds.push(value[1]);
                    } else if(index !== 0 && emailIds.indexOf(value[1]) > -1){
                      console.log(value);
                      errorArray.push(value[1]+" already in the list, removed the duplicate");
                      }else if(index !== 0 && value[0] == undefined && value[1] != undefined){
                      console.log(value);
                      errorArray.push("No name found for "+value[1]+" Please add that employee manually");
                      }
                    } else if(index !== 0 && value[0] != undefined && value[1] == undefined){
                      errorArray.push("No email found for "+value[0]+", please add that employee manually")
                    }
                });
                errorArray.forEach((value)=>{
                  this.notifier.notify('error', value);
                })
                this.onTest();
                console.log('emailIds', emailIds);
                if (modifiedData.length > 0) {
                    modifiedData.forEach((value, index)=>{
                      value.id = index
                    })
                    this.getExelData = true;
                    this.excelData = modifiedData;
                    this.dataSource = new MatTableDataSource(modifiedData);;
                    this.dataSource.sort = this.sort;
                    this.dataSource.paginator = this.paginator;
                    this.showExcelModal();
                }else{
                  this.getExelData = false;
                }
                console.log('modifiedData', this.dataSource);
                // this.headerAsObject = this.data[0];
                // for(let key in this.headerAsObject){
                //   this.headers.push(this.headerAsObject[key])
                // }
                // console.log( this.headers , 'this headers')
            } else {
                this.excelSheetPlayGround = false;
            }
            //console.log("data", this.data);
        };

        reader.readAsBinaryString(target.files[0]);
    }
    onHidden(): void {
        this.isExcelModalShown = false;
        (<HTMLInputElement>document.getElementById('fileSelect')).value = '';
    }
    onTest(){
      console.log("working notifier");
    }
    onExcelEdit(dataIndex) {
      console.log('editing');
      console.log(dataIndex);
        let that = this;
        this.excelData.map(function (value, index) {
            if (index == dataIndex) {
                that.excelData[index]['edit'] = !value.edit;
            }
        });
    }
    deleteFromExcelData(recordIndex) {
        this.excelData = this.excelData.filter(function (value, index) {
            return index !== recordIndex;
        });
    }

    finalExcelSubmit() {
      console.log('start');
        let newRes = this.newRespondants;
        let isValid = true;
        this.excelData.map(function(value, index) {
            if(value.nameError || value.emailError) {
                isValid = false;
            }
        });
        if(!isValid) {
            Swal.fire('Error Uploading Excel', 'Please verify data and try again', 'warning');
            // swal({
            //     title: 'Error Uploading Excel',
            //     text: 'Please verify data and try again',
            //     type: 'warning',
            //     showConfirmButton: true
            // });
            return false;
        }
        let emailIds = this.newRespondants.map(function (value, index) {
            return value.emailId;
        });
        console.log('emailIds', emailIds);
        this.excelData.map(function (value, index) {
            if (emailIds.indexOf(value.email) == -1) {
                newRes.push({
                    'emailId': value.email,
                    'name': value.name
                });
            } else{
              // console.log(value.email);
            }
        });
        this.newRespondants = newRes;
        this.respondants = newRes;
        this.onHidden();
    }
    showNotification( type: string, message: string ): void {
    this.notifier.notify( type, message );
    }
    onExcelInputChange(evt: any, dataIndex, objectIndex) {
        let that = this;
        let regExFormatEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let fieldValue = evt.target.value;
        this.excelData.map(function (value, index) {
            if (index == dataIndex) {
                that.excelData[index][objectIndex] = fieldValue;
                switch (objectIndex) {
                    case 'employeeName':
                        if (fieldValue == '') {
                            that.excelData[index][objectIndex + 'Error'] = true;
                        }
                        else {
                            that.excelData[index][objectIndex + 'Error'] = false;
                        }
                        break;
                    case 'employeeEmail':
                        if (regExFormatEmail.test(fieldValue) == false || fieldValue == '') {
                            that.excelData[index][objectIndex + 'Error'] = true;
                        }
                        else {
                            that.excelData[index][objectIndex + 'Error'] = false;
                        }
                        break;
                    case 'managerName':
                        if (fieldValue == '') {
                            that.excelData[index][objectIndex + 'Error'] = true;
                        }
                        else {
                            that.excelData[index][objectIndex + 'Error'] = false;
                        }
                        break;
                    case 'managerEmail':
                        if (regExFormatEmail.test(fieldValue) == false || fieldValue == '') {
                            that.excelData[index][objectIndex + 'Error'] = true;
                        }
                        else {
                            that.excelData[index][objectIndex + 'Error'] = false;
                        }
                        break;    
                }
            }
        });
    }
    hasWhiteSpace(s) {
      return /^\s+$/.test(s);
    }
    launchSurvey(){
       console.log(this.hasWhiteSpace(this.firstFormGroup.value.firstCtrl));
        console.log(this.templateId);
        console.log(this.firstFormGroup.value.firstCtrl);
        console.log(this.firstFormGroup.value.date);
        console.log(this.secondFormGroup.value.file);
        console.log(this.dataSource.filteredData);
      if(this.firstFormGroup.value.firstCtrl == ''){
         console.log("added");
         this.notifier.notify('error', "Please enter company name");
      } else if(this.hasWhiteSpace(this.firstFormGroup.value.firstCtrl) == true){
         this.notifier.notify('error', "Please enter company name");
      } else{
        if(this.getExelData){
          if(this.findSECompayId){
            let data = {
            templateId : this.templateId.id,
            companyName: this.companyNameFromChild,
            companySeId: this.companyID,
            surveyStartDate: this.firstFormGroup.value.date.begin,
            surveyEndDate: this.firstFormGroup.value.date.end,
            employeeData: this.dataSource.filteredData
            }
            console.log(data);
            this.adminService.launchSurvey(data).subscribe(response=>{
              if(response.status){
                Swal.fire({
                  icon: 'success',
                  title: 'Successfully saved',
                  showConfirmButton: false,
                })
                this.router.navigate(['admin/dashboard']);
              } else{
                Swal.fire({
                  icon: 'error',
                  title: 'Something went wrong',
                  showConfirmButton: false,
                })
              }
            })
          } else{
            let data = {
            templateId : this.templateId.id,
            companyName: this.companyNameFromChild,
            companySeId: 6,
            surveyStartDate: this.firstFormGroup.value.date.begin,
            surveyEndDate: this.firstFormGroup.value.date.end,
            employeeData: this.dataSource.filteredData
            }
            this.adminService.launchSurvey(data).subscribe((response: any)=>{
              console.log(response)
              if(response.status){
                Swal.fire({
                  icon: 'success',
                  title: 'Successfully saved',
                  showConfirmButton: false,
                })
                this.router.navigate(['admin/dashboard']);
              } else{
                Swal.fire({
                  icon: 'error',
                  title: 'Something went wrong',
                  showConfirmButton: false,
                })
              }
            })
          }

          
        }
      }
     
        
    }
    getCompanyOutputValFromChild(selected: string) {
    if (selected) {
      this.companyNameFromChild = selected;
      this.newCompanyNameFromChild = '';
      this.firstFormGroup.value.firstCtrl = this.companyNameFromChild;
    }
  }

  gettIsNewCompanyFromParent(isNew: boolean) {
    if (isNew) {
      this.newCompanyNameFromChild = this.companyNameFromChild;
      this.firstFormGroup.value.firstCtrl = this.companyNameFromChild;
    }
  }
  companyListStatus(event){
    console.log(event);
    if(event == true){
      this.isLoading = false;
    }
  }
  getCompanyOutputIdFromChild(id: string) {
      if(id != null){
        this.adminService.getSingleCompany(id).subscribe((response: any) => {
          if(response.status){
            this.companyID = id;
            this.findSECompayId = true;
          } else{
            this.findSECompayId = false;
          }
        });
      } else{
        this.findSECompayId = false;
      }
  }

  emptyEmployeeData(){
    this.excelData = [];
    this.isExcelModalShown = false;
    this.getExelData = false;
    this.newRespondants = [];
    this.respondants = [];
    this.excelType = false;
    this.data = [];
    this.dataSource.filteredData = [];
    this.table.renderRows();
  }
  getGMCEmployees(){
    let data={
      seCompanyId: this.companyID,
      companyName: this.companyNameFromChild
    }
    this.adminService.getGMCEmployees(data).subscribe(response=>{
      console.log(response);
      if(response.status){
        for(let i =0; i < response.data.length; i++){
          const found = this.dataSource.filteredData.some(el => el.employeeEmail === response.data[i].employeeEmail);
          if (!found){
            this.dataSource.filteredData.push(response.data[i]);
          } else{
            console.log("Already exists "+ response.data[i].employeeEmail)
          }
        }
        console.log(this.dataSource.filteredData);
        this.dataSource.filteredData.forEach((value, index)=>{
          value.id = index
        })
        // for(let i=0; i< this.dataSource.filteredData.length; i++){
        //   this.dataSource.filteredData[i].id = i;
        // }
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.renderRows();
      }
    })
  }
  
}


