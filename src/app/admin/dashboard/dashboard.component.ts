import { Component, OnInit } from '@angular/core';
import { SidenavService } from '../sidenav.service';
import { onMainContentChange, animateText, onSideNavChange } from '../animation';
import { Router } from "@angular/router";
import * as jwt_decode from "jwt-decode";
interface Page {
  link: string;
  name: string;
  icon: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [ onMainContentChange,animateText, onSideNavChange ]
})
export class DashboardComponent implements OnInit {
	name = 'Angular';
  public onSideNavChange: boolean;
  isExpanded: boolean = false;
  public sideNavState: boolean = false;
  public linkText: boolean = false;
  postDecode: any;
  received_token: any;

  public pages: Page[] = [
    {name: 'Survey Management', link:'some-link', icon: 'home'}
  ]
  constructor(private _sidenavService: SidenavService,
    private router: Router,) { 
  	this._sidenavService.sideNavState$.subscribe( res => {
      console.log(res)
      this.onSideNavChange = res;
    })
  }

  ngOnInit(): void {
    let tokenInfo = this.getDecodedAccessToken(localStorage.getItem("token"));
    if(tokenInfo == null){
      this.router.navigate(['/admin']);
    }
    this.postDecode = JSON.stringify(tokenInfo);
    this.received_token = JSON.parse(this.postDecode);    
    if(tokenInfo != null){
      if(this.received_token.role != "Admin"){
        this.router.navigate(['/admin']);
      }
    }
  }
  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }
  onSinenavToggle() {
    this.sideNavState = !this.sideNavState
    
    setTimeout(() => {
      this.linkText = this.sideNavState;
    }, 200)
    this._sidenavService.sideNavState$.next(this.sideNavState)
  }
  routing(pageName){
    if(pageName == 'dashboard'){      
      this.router.navigate(["/admin/dashboard"]);
    }
  }
  logout(){
    localStorage.clear();
    this.router.navigate(["/admin"]);
  }

}
