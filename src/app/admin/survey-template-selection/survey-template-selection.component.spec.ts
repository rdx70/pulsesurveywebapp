import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyTemplateSelectionComponent } from './survey-template-selection.component';

describe('SurveyTemplateSelectionComponent', () => {
  let component: SurveyTemplateSelectionComponent;
  let fixture: ComponentFixture<SurveyTemplateSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyTemplateSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyTemplateSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
