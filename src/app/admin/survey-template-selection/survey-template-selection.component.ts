import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-survey-template-selection',
  templateUrl: './survey-template-selection.component.html',
  styleUrls: ['./survey-template-selection.component.css']
})
export class SurveyTemplateSelectionComponent implements OnInit {
  templateList: any = [];
  isLoading = false;
  constructor(private adminService: AdminService) { }

  ngOnInit(){
    this.isLoading = true;
  	this.adminService.getSurveyTemplate().subscribe((response: any)=>{
  		if(response.status){
        this.isLoading = false;
  			this.templateList = response.data;
  			console.log(this.templateList);
  		}
  	});
  }

}
