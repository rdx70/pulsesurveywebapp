import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyConfigurationComponent } from './survey-configuration.component';

describe('SurveyConfigurationComponent', () => {
  let component: SurveyConfigurationComponent;
  let fixture: ComponentFixture<SurveyConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
