import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../admin.service';
import { MatSort } from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { dateFormat, getCustomDate, dayAdd } from "../../config/utils";
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';
import { MatTable } from '@angular/material/table';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
const ELEMENT_DATA: any = [];
import Swal from 'sweetalert2';
@Component({
  selector: 'app-survey-configuration',
  templateUrl: './survey-configuration.component.html',
  styleUrls: ['./survey-configuration.component.css']
})
export class SurveyConfigurationComponent implements OnInit {
  surveyId: any;
  participantList = [];
  surveyStartDate: any;
  surveyEndDate: any;
  isLoading =  false;
  today = dayAdd(Date.now(), "day", 1);
  maxDate = dayAdd(Date.now(), "day", 7);
  firstFormGroup: FormGroup;
  constructor(
  	private route: ActivatedRoute,
       private router: Router,
       private adminService: AdminService,
       public dialog: MatDialog,
       private _formBuilder: FormBuilder) { }

  displayedColumns: string[] = ['id','employeeName', 'employeeEmail', 'managerName', 'managerEmail','surveySubmittedStatus', 'action'];
  dataSource: any = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatTable,{static:true}) table: MatTable<any>;
  ngOnInit() {
    this.isLoading = true;
  	this.surveyId = { id: this.route.snapshot.params['id'] };
  	
  	this.adminService.getParticipant(this.surveyId).subscribe(response=>{
  		console.log(response);
  		if(response.status){
        console.log(response);
        this.surveyStartDate = response.data[0].surveyId.surveyStartDate;
        this.surveyEndDate = response.data[0].surveyId.surveyEndDate;
        this.firstFormGroup = this._formBuilder.group({
        date: [{begin: this.surveyStartDate, end: this.surveyEndDate}]
      });
        response.data.forEach((value, index)=>{
        var surveySubmittedStatus;
           if(value.surveySubmittedStatus){
             surveySubmittedStatus = "Submitted"
           } else{
             surveySubmittedStatus = "Not Submitted"
           }
           let data = {
             id: index,
             employeeName: value.employeeName,
             employeeEmail: value.employeeEmail,
             managerName: value.managerName,
             managerEmail: value.managerEmail,
             _id: value._id,
             surveyId: value.surveyId,
            surveySubmittedStatus: surveySubmittedStatus
           }
           this.participantList.push(data);
        })
      this.dataSource = new MatTableDataSource(this.participantList);
      console.log(this.dataSource);
      this.isLoading = false;
    } else{
      Swal.fire('Oops...', 'Something went wrong' , 'error')
    }
  	})
  }
  openDialog(action,obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data:obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.event == 'Add'){
        this.addRowData(result.data);
      }else if(result.event == 'Update'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData(result.data);
      }
    });
  }

  addRowData(row_obj){
    const found = this.dataSource.filteredData.some(el => el.employeeEmail === row_obj.employeeEmail);
    if (found){
      console.log(found)
      Swal.fire({
              icon: 'info',
              title: row_obj.employeeEmail+ ' is already in the list',
              showConfirmButton: true,
            })
    } else{
    var id = this.dataSource.length;
    this.dataSource.filteredData.push({
      id: id,
      employeeName:row_obj.employeeName,
      employeeEmail:row_obj.employeeEmail,
      managerName:row_obj.managerName,
      managerEmail:row_obj.managerEmail
    });
    console.log(row_obj);
    let data = {
      employeeName:row_obj.employeeName,
      employeeEmail:row_obj.employeeEmail,
      managerName:row_obj.managerName,
      managerEmail:row_obj.managerEmail,
      surveyId: this.surveyId.id
    }
    this.adminService.addParticipant(data).subscribe(response=>{
      if(response.status){
        this.dataSource.filteredData.forEach((value, index)=>{
           value.id = index
        })
        this.table.renderRows();
            Swal.fire({
              icon: 'success',
              title: 'Successfuly updated',
              showConfirmButton: false,
            })
          } else{
            Swal.fire({
              icon: 'error',
              title: 'Something went wrong',
              showConfirmButton: false,
            })
          }
    })
    }
    
  }
  updateRowData(row_obj){
    console.log(this.dataSource);
    this.dataSource.filteredData = this.dataSource.filteredData.filter((value,key)=>{
      if(value.id == row_obj.id){
        value.employeeName = row_obj.employeeName;
        value.employeeEmail = row_obj.employeeEmail;
        value.managerName = row_obj.managerName;
        value.managerEmail = row_obj.managerEmail;
        console.log(row_obj);
        this.adminService.updateParticipant(row_obj).subscribe(response=>{
          console.log(response)
          if(response.status){
            Swal.fire({
              icon: 'success',
              title: 'Successfuly updated',
              showConfirmButton: false,
            })
          } else{
            Swal.fire({
              icon: 'error',
              title: 'Something went wrong',
              showConfirmButton: false,
            })
          }
        })
      }
      return true;
    });
  }
  deleteRowData(row_obj){
    console.log(row_obj);
    var ind = this.dataSource.filteredData.findIndex(function(element){
       return element.id===row_obj.id;
    })
    if(ind!==-1){
    this.dataSource.filteredData.splice(ind, 1)
    }
    this.adminService.deleteParticipant(row_obj._id).subscribe(response=>{
          if(response.status){
            console.log(response);
            if(response.status){
              this.dataSource.filteredData.forEach((value, index)=>{
               value.id = index
            })
              this.table.renderRows();
            Swal.fire({
              icon: 'success',
              title: 'Successfuly deleted',
              showConfirmButton: false,
            })
          } else{
            Swal.fire({
              icon: 'error',
              title: 'Something went wrong',
              showConfirmButton: false,
            })
          }
          }
        })
    
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  updateSurvey(){
    let data={
      _id: this.surveyId.id,
      surveyStartDate: this.firstFormGroup.value.date.begin,
      surveyEndDate: this.firstFormGroup.value.date.end,
    }
    this.adminService.updateSurvey(data).subscribe(response=>{
      console.log(response)
      if(response.status){
        Swal.fire({
              icon: 'success',
              title: 'Successfuly updated',
              showConfirmButton: false,
            })
      } else{
        Swal.fire({
              icon: 'error',
              title: 'Something went wrong',
              showConfirmButton: false,
            })
      }
    })
  }

}
