import { Component, OnInit, EventEmitter, Output,ChangeDetectionStrategy } from '@angular/core';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { debounceTime } from 'rxjs/operators';
import { distinctUntilChanged } from 'rxjs/operators';

import { AdminService } from './admin.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-company-input',
  template: `
              <div class="form-group col-md-12 ptb-15">
                <mat-form-field>
                      <input
                      type="text" 
                      placeholder="Search & select your current company" 
                      aria-label="Number" 
                      matInput
                      name="company"
                      [formControl]="myControl"
                      [matAutocomplete]="auto"
                      matInput
                      (change)="companyAddedByChange()"
                      [value] = "data"
                      required>
                      </mat-form-field>
                <mat-autocomplete autoActiveFirstOption #auto="matAutocomplete">
                  <mat-option *ngIf="isLoading" class="is-loading">
                    <mat-spinner diameter="50"></mat-spinner>
                  </mat-option>
                  <ng-container *ngIf="!isLoading">
                    <mat-option 
                      *ngFor="let option of filteredOptions | async" [value]="option"
                      (onSelectionChange)="companyAdded($event)">
                      {{ option }}
                    </mat-option>
                  </ng-container>
                </mat-autocomplete>
                <p class="help-block"
                *ngIf="newCompanyAlert">
                  You have added a new company out of this list
                </p>
             </div>`,
  styleUrls: ['./survey-management/survey-management.component.css']
})
export class CompanyInputComponent implements OnInit {
  myControl: FormControl = new FormControl();
  options = [];
  idArray = [];
  filteredOptions: Observable<string[]>;
  newCompanyAlert: boolean = false;
  result: any;
  companyId: any;
  data = '';
  isNullCompanyValid: boolean;
  isLoading = false;
  isLoading1 = true;
  isRegisteredByCompany: boolean;
  @Output()
  outPutId = new EventEmitter<string>();
  @Output()
  outPutToParent = new EventEmitter<string>();
  @Output()
  outPutIsNewCompanyToParent = new EventEmitter<boolean>();
  @Output()
  companyListStatus = new EventEmitter<boolean>();

  constructor(
    private surveyEngineService: AdminService,
  ) {}
  
  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      debounceTime(1000),
      distinctUntilChanged(),
      map((val) => {
        console.log(val);
        this.isLoading = true;
        if (val != '') {
          this.isLoading = false;
          return this.filter(val);
        }
        this.isLoading = false;
        return null;
      })
    );
    this.surveyEngineService.getAllCompanies().subscribe((response: any) => {
      console.log(response);
      if(response.status){
        this.companyListStatus.emit(true);
        console.log(this.isLoading1)
        this.isLoading1 = false;
        console.log(this.isLoading1)
        response.data.forEach(company => {
        this.options.push(company.name);
        this.idArray.push(company.id);
      });
      }
    });
  }
  companyAdded(val) {
    //Sending Data to parent Form
    let isNew = this.checkNewCompany(val.source.value);
    if (isNew) {
      this.outPutToParent.emit(val.source.value);
      this.outPutId.emit(this.companyId);
    } else {
      this.outPutToParent.emit(this.options[this.result]);
      this.outPutId.emit(this.companyId);
    }
    this.outPutIsNewCompanyToParent.emit(isNew);
    this.outPutId.emit(this.companyId);
  }
  companyAddedByChange() {
    if(this.myControl.value.length <=0){
    } else{
      //Sending Data to parent Form
    let isNew = this.checkNewCompany(this.myControl.value);
    if (isNew) {
      this.outPutToParent.emit(this.myControl.value);
      this.outPutId.emit(this.companyId);
    } else {
      this.outPutToParent.emit(this.options[this.result]);
      this.outPutId.emit(this.companyId);
    }
    this.outPutIsNewCompanyToParent.emit(isNew);
    this.outPutId.emit(this.companyId);
    }
    
  }

  checkNewCompany(value) {
    
    this.result = this.options.findIndex(
      item => value.toLowerCase() == item.toLowerCase()
    );

    if(this.result >= 0){
      this.companyId = this.idArray[this.result];
    } else{
      this.companyId = null;
    }

    if (this.result != -1) {
      //In array
      this.newCompanyAlert = false;
      return this.newCompanyAlert;
    } else {
      //Not in array
      this.newCompanyAlert = true;
      return this.newCompanyAlert;
    }
  }

  filter(val: string): string[] {
    return this.options.filter(
      option => option.toLowerCase().indexOf(val.toLowerCase()) === 0
    );
  }
}
