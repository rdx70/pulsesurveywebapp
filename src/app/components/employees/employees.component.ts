import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/api.service';
import { LocalDataSummary} from 'src/app/pulsesurveydata';
import { employeeData } from '../../../assets/data/pulsesurveydata';
@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
})
export class EmployeesComponent implements OnInit {
  data: LocalDataSummary[];
  employees: string[] = [];

  
  
  constructor(private service: ApiService) {}

  ngOnInit(): void {
    this.service.getLocalData().subscribe((result) => {
      this.data = result;
      this.data.forEach((cs) => {
        this.employees.push(cs.ManagersName);
      })
    })
    }
}

