import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { employeeData } from '../../../assets/data/pulsesurveydata';

import { LocalDataSummary } from 'src/app/pulsesurveydata';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  totalBusinessContinuity = 0;
  totalWellbeing = 0;
  totalApproachabilityPsychologicalavailability = 0;
  totalLearningOrientation = 0;
  totalTeamConnect = 0;
  totalSupport = 0;
  totalPersonalConnect = 0;
  totalCommunication = 0;
  totalCelebration = 0;

  totalEmpathy = 0;
  totalParticipativeManagement = 0;
  totalWorklifebalance = 0;
  totalEffectiveness = 0;
  totalProductivity = 0;
  totalReadiness = 0;

  totalGrandmean = 0;

  localData: LocalDataSummary[];

  ngOnInit(): void {
    this.localData = employeeData;
    employeeData.forEach((cs) => {
      this.totalBusinessContinuity = Math.round((618 / 684) * 100);
      this.totalWellbeing = Math.round((570 / 663) * 100);
      this.totalApproachabilityPsychologicalavailability = Math.round(
        (514 / 637) * 100
      );

      this.totalLearningOrientation = Math.round((588 / 673) * 100);
      this.totalTeamConnect = Math.round((453 / 600) * 100);
      this.totalSupport = Math.round((521 / 637) * 100);
      this.totalPersonalConnect = Math.round((399 / 574) * 100);
      this.totalCommunication = Math.round((514 / 630) * 100);
      this.totalCelebration = Math.round((417 / 566) * 100);

      this.totalEmpathy = Math.round((454 / 596) * 100);
      this.totalParticipativeManagement = Math.round((511 / 622) * 100);
      this.totalWorklifebalance = Math.round((398 / 571) * 100);
      this.totalEffectiveness = Math.round((453 / 609) * 100);
      this.totalProductivity = Math.round((421 / 575) * 100);
      this.totalReadiness = Math.round((600 / 654) * 100);
      this.totalGrandmean = Math.round((1195 / 1500) * 100);
    });
  }
}
