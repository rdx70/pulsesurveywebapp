import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-card',
  templateUrl: './dashboard-card.component.html',
  styleUrls: ['./dashboard-card.component.css']
})
export class DashboardCardComponent implements OnInit {

  @Input('totalBusinessContinuity')
  totalBusinessContinuity;
  @Input('totalWellbeing')
  totalWellbeing;
  @Input('totalApproachabilityPsychologicalavailability')
  totalApproachabilityPsychologicalavailability;
  @Input('totalLearningOrientation')
  totalLearningOrientation;
  @Input('totalTeamConnect')
  totalTeamConnect;
  @Input('totalSupport')
  totalSupport;
  @Input('totalPersonalConnect')
  totalPersonalConnect;
  @Input('totalCommunication')
  totalCommunication;
  @Input('totalCelebration')
  totalCelebration;
  @Input('totalEmpathy')
  totalEmpathy;
  @Input('totalParticipativeManagement')
  totalParticipativeMgt;
  @Input('totalWorklifebalance')
  totalWorklifebalance;
  @Input('totalEffectiveness')
  totalEffectiveness;
  @Input('totalProductivity')
  totalProdcutivity;
  @Input('totalReadiness')
  totalReadiness;
  @Input('totalGrandmean')
  totalGrandmean;
  
  

  constructor() { }

  ngOnInit(): void {
  }

}