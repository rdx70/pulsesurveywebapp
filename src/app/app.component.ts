import { Component, ViewChild } from '@angular/core';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexTitleSubtitle,
} from 'ng-apexcharts';
export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  title: ApexTitleSubtitle;
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'pulsesurveydashboard';
  @ViewChild("chart", { static: false }) chart: ChartComponent;
  public chartOptions: Partial<any>;

  constructor() {
    this.chartOptions = {
      series: [
        {
          name: 'Score',
          data: [90, 86, 81, 87, 76, 82, 70, 82, 74, 76, 82, 70, 74, 73, 92],
        },
      ],
      chart: {
        height: 350,
        type: 'bar',
      },
      title: {
        text: 'Scores',
      },
      xaxis: {
        categories: [
          'Business Continuity',
          'Wellbeing',
          'Approachability/Psychological availability',
          'Learning Orientation',
          'Team Connect',
          'Support',
          'Personal Connect',
          'Communication',
          'Celebration',
          'Empathy',
          'Participative Management',
          'Work-life balance',
          'Effectiveness',
          'Productivity',
          'Readiness',
        ],
      }
    };
  }
}

  