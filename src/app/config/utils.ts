export const getCustomDate = action => {
  let d = new Date();
  let year = d.getFullYear();
  let month = d.getMonth();
  let day = d.getDate();
  let c = new Date(year + 1, month, day);

  if (action === "year") {
    return year;
  }
  if (action === "month") {
    return month;
  }
  if (action === "date") {
    return d;
  }
  if (action === "oneYearDate") {
    return c;
  }
};
export const dayAdd = (date, interval, units) => {
  var ret = new Date(date); //don't change original date
  var checkRollover = function() {
    if (ret.getDate() != date.getDate()) ret.setDate(0);
  };
  switch (interval.toLowerCase()) {
    case "year":
      ret.setFullYear(ret.getFullYear() + units);
      checkRollover();
      break;
    case "quarter":
      ret.setMonth(ret.getMonth() + 3 * units);
      checkRollover();
      break;
    case "month":
      ret.setMonth(ret.getMonth() + units);
      // checkRollover();
      break;
    case "week":
      ret.setDate(ret.getDate() + 7 * units);
      break;
    case "day":
      ret.setDate(ret.getDate() + units);
      break;
    case "hour":
      ret.setTime(ret.getTime() + units * 3600000);
      break;
    case "minute":
      ret.setTime(ret.getTime() + units * 60000);
      break;
    case "second":
      ret.setTime(ret.getTime() + units * 1000);
      break;
    default:
      ret = undefined;
      break;
  }
  return ret;
};
export const dateFormat = (date, format) => {
  let d = new Date(date);
  let year = d.getFullYear();
  let month = d.getMonth();
  let day = d.getDate();
  //  let oneDayAfter : Date ;
  let dateFormat = "";
  let months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];
  let monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  if (format == "dd/mm/yyyy") {
    dateFormat = `${day}/${month + 1}/${year}`;
    return dateFormat;
  }
  // if(format == 'oneDayAfter'){
  //   return oneDayAfter = new Date(day + 1, month+1, year)
  // }
  if (format == "dd-mm-yyyy") {
    dateFormat = `${day}-${month + 1}-${year}`;
    return dateFormat;
  }
  if (format == "ddmmmyyyy") {
    dateFormat = `${day} ${months[month]} ${year}`;
    return dateFormat;
  }
  if (format == "ddmmmmyyyy") {
    dateFormat = `${day} ${monthNames[month]} ${year}`;
    return dateFormat;
  }
};

export const getMessage = (manager, surveys) => {
  let message = "";
  if (manager) {
    if (manager.managerInfo.managerModerationStatus == "certified") {
      return (message = "sas_tfs_audited_certified");
    }
    if (manager.managerInfo.managerModerationStatus == "rejected") {
      return (message = "sas_tfs_audited_not_certified");
    }
    if (manager.managerInfo.managerModerationStatus == "audit_in_progress") {
      return (message = "sas_tfs_audit_in_progress");
    }
    if (
      surveys.length > 0 &&
      manager.managerInfo.managerModerationStatus == null
    ) {
      if (
        new Date(surveys[surveys.length - 1].startDate) >= new Date(Date.now())
      ) {
        return (message = "sas_tfs_launched");
      }
      if (
        new Date(surveys[surveys.length - 1].startDate) <=
          new Date(Date.now()) &&
        new Date(surveys[surveys.length - 1].endDate) >= new Date(Date.now())
      ) {
        return (message = "sas_tfs_ongoing");
      }
      if (
        new Date(surveys[surveys.length - 1].endDate) <= new Date(Date.now())
      ) {
        return (message = "sas_tfs_closed");
      }
      // if(manager.scores){
      //   if(manager.scores.overall < 90){
      //     return message = 'sas_tfs_not_eligible';
      //    }
      // }
    } else {
      return (message = "sas");
    }
  }
};

export const isEmpty = value =>
  value === undefined ||
  value === null ||
  (typeof value === "object" && Object.keys(value).length === 0) ||
  (typeof value === "string" && value.trim().length === 0);

export const getCloudData = type => {
  let data = [];
  if (type == "CH+DL") {
    return (data = [
      { text: "Autocratic", weight: 2 },
      { text: "Persuasive", weight: 4 },
      { text: "Democratic", weight: 6 },
      { text: "Laissez-faire", weight: 3 },
      { text: "Collaborative", weight: 7 },
      { text: "Affiliative", weight: 8 },
      { text: "Charismatic", weight: 6 },
      { text: "Inspirational", weight: 5 },
      { text: "Disciplinarian", weight: 5 },
      { text: "Motivator", weight: 5 },
      { text: "Coach", weight: 4 },
      { text: "Consultative", weight: 6 }
    ]);
  }
  if (type == "CH+IL") {
    return (data = [
      { text: "Persuasive", weight: 4 },
      { text: "Democratic", weight: 6 },
      { text: "Laissez-faire", weight: 5 },
      { text: "Collaborative", weight: 7 },
      { text: "Affiliative", weight: 8 },
      { text: "Charismatic", weight: 3 },
      { text: "Inspirational", weight: 1 },
      { text: "Disciplinarian", weight: 5 },
      { text: "Motivator", weight: 3 },
      { text: "Coach", weight: 5 },
      { text: "Consultative", weight: 6 }
    ]);
  }
  if (type == "CH+DL+IL") {
    return (data = [
      { text: "Autocratic", weight: 1 },
      { text: "Persuasive", weight: 3 },
      { text: "Democratic", weight: 6 },
      { text: "Laissez-faire", weight: 3 },
      { text: "Chaotic", weight: 5 },
      { text: "Collaborative", weight: 7 },
      { text: "Affiliative", weight: 8 },
      { text: "Charismatic", weight: 3 },
      { text: "Inspirational", weight: 1 },
      { text: "Disciplinarian", weight: 5 },
      { text: "Motivator", weight: 3 },
      { text: "Coach", weight: 4 },
      { text: "Consultative", weight: 6 }
    ]);
  }
  if (type == "IH+CL") {
    return (data = [
      { text: "Autocratic", weight: 6 },
      { text: "Persuasive", weight: 8 },
      { text: "Democratic", weight: 3 },
      { text: "Laissez-faire", weight: 4 },
      { text: "Collaborative", weight: 3 },
      { text: "Affiliative", weight: 1 },
      { text: "Charismatic", weight: 6 },
      { text: "Inspirational", weight: 8 },
      { text: "Disciplinarian", weight: 4 },
      { text: "Motivator", weight: 6 },
      { text: "Coach", weight: 4 },
      { text: "Consultative", weight: 6 }
    ]);
  }
  if (type == "IH+DL") {
    return (data = [
      { text: "Persuasive", weight: 5 },
      { text: "Democratic", weight: 4 },
      { text: "Laissez-faire", weight: 2 },
      { text: "Chaotic", weight: 8 },
      { text: "Collaborative", weight: 2 },
      { text: "Affiliative", weight: 5 },
      { text: "Charismatic", weight: 5 },
      { text: "Inspirational", weight: 8 },
      { text: "Disciplinarian", weight: 3 },
      { text: "Motivator", weight: 5 },
      { text: "Coach", weight: 1 },
      { text: "Consultative", weight: 4 }
    ]);
  }
  if (type == "IH+CL+DL") {
    return (data = [
      { text: "Autocratic", weight: 7 },
      { text: "Persuasive", weight: 5 },
      { text: "Democratic", weight: 4 },
      { text: "Laissez-faire", weight: 2 },
      { text: "Chaotic", weight: 9 },
      { text: "Collaborative", weight: 2 },
      { text: "Strategic", weight: 3 },
      { text: "Affiliative", weight: 1 },
      { text: "Charismatic", weight: 4 },
      { text: "Inspirational", weight: 9 },
      { text: "Motivator", weight: 3 },
      { text: "Coach", weight: 3 },
      { text: "Consultative", weight: 3 }
    ]);
  }
  if (type == "CL+DH+IH") {
    return (data = [
      { text: "Autocratic", weight: 5 },
      { text: "Persuasive", weight: 6 },
      { text: "Democratic", weight: 1 },
      { text: "Laissez-faire", weight: 9 },
      { text: "Chaotic", weight: 3 },
      { text: "Collaborative", weight: 7 },
      { text: "Strategic", weight: 6 },
      { text: "Affiliative", weight: 4 },
      { text: "Charismatic", weight: 6 },
      { text: "Inspirational", weight: 8 },
      { text: "Motivator", weight: 7 },
      { text: "Coach", weight: 6 }
    ]);
  }
  if (type == "DL+CH+IH") {
    return (data = [
      { text: "Autocratic", weight: 3 },
      { text: "Persuasive", weight: 3 },
      { text: "Democratic", weight: 7 },
      { text: "Laissez-faire", weight: 1 },
      { text: "Chaotic", weight: 6 },
      { text: "Collaborative", weight: 4 },
      { text: "Strategic", weight: 4 },
      { text: "Affiliative", weight: 8 },
      { text: "Charismatic", weight: 8 },
      { text: "Inspirational", weight: 8 },
      { text: "Disciplinarian", weight: 7 },
      { text: "Coach", weight: 3 },
      { text: "Data Driven", weight: 3 }
    ]);
  }
  if (type == "IL+CH+DH") {
    return (data = [
      { text: "Autocratic", weight: 1 },
      { text: "Persuasive", weight: 1 },
      { text: "Democratic", weight: 8 },
      { text: "Laissez-faire", weight: 7 },
      { text: "Chaotic", weight: 2 },
      { text: "Collaborative", weight: 6 },
      { text: "Strategic", weight: 7 },
      { text: "Affiliative", weight: 7 },
      { text: "Charismatic", weight: 3 },
      { text: "Inspirational", weight: 1 },
      { text: "Motivator", weight: 4 },
      { text: "Coach", weight: 7 },
      { text: "Data Driven", weight: 8 }
    ]);
  }
  if (type == "M") {
    return (data = [
      { text: "Autocratic", weight: 1 },
      { text: "Persuasive", weight: 3 },
      { text: "Democratic", weight: 5 },
      { text: "Laissez-faire", weight: 5 },
      { text: "Chaotic", weight: 1 },
      { text: "Collaborative", weight: 5 },
      { text: "Strategic", weight: 8 },
      { text: "Affiliative", weight: 5 },
      { text: "Charismatic", weight: 4 },
      { text: "Inspirational", weight: 4 },
      { text: "Motivator", weight: 5 },
      { text: "Data Driven", weight: 7 }
    ]);
  }
  if (type == "L") {
    return (data = [
      { text: "Autocratic", weight: 10 },
      { text: "Democratic", weight: 6 },
      { text: "Laissez-faire", weight: 5 },
      { text: "Chaotic", weight: 9 },
      { text: "Collaborative", weight: 4 },
      { text: "Charismatic", weight: 1 },
      { text: "Inspirational", weight: 3 },
      { text: "Disciplinarian", weight: 8 },
      { text: "Motivator", weight: 2 },
      { text: "Coach", weight: 3 },
      { text: "Consultative", weight: 7 }
    ]);
  }
  if (type == "H") {
    return (data = [
      { text: "Autocratic", weight: 1 },
      { text: "Persuasive", weight: 4 },
      { text: "Democratic", weight: 7 },
      { text: "Laissez-faire", weight: 7 },
      { text: "Chaotic", weight: 2 },
      { text: "Collaborative", weight: 6 },
      { text: "Strategic", weight: 6 },
      { text: "Affiliative", weight: 1 },
      { text: "Inspirational", weight: 10 },
      { text: "Disciplinarian", weight: 3 },
      { text: "Motivator", weight: 8 }
    ]);
  }
};

export const timeFormatForVideoPlayer = seconds => {
  let m: any;
  m =
    Math.floor(seconds / 60) < 10
      ? "0" + Math.floor(seconds / 60)
      : Math.floor(seconds / 60);
  let s =
    Math.floor(seconds - m * 60) < 10
      ? "0" + Math.floor(seconds - m * 60)
      : Math.floor(seconds - m * 60);
  return m + ":" + s;
};
