export interface LocalDataSummary {
    ManagersName ?: string,
    
    BusinessContinuity  ?: number,
    Wellbeing  ?: number,
    ApproachabilityPsychologicalavailability  ?: number,
    LearningOrientation  ?: number,
    TeamConnect  ?: number,
    Support  ?: number,
    PersonalConnect  ?: number,
    Communication  ?: number,
    Celebration  ?: number,

    Empathy  ?: number,
    ParticipativeMgt  ?: number,
    Worklifebalance  ?: number,
    Effectiveness  ?: number,
    Productivity  ?: number,
    Readiness  ?: number,
    
    }