import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators'
import {LocalDataSummary} from './pulsesurveydata';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private localDataUrl = 'assets/data/pulsesurveydata';

  constructor(private http : HttpClient) { }

  getLocalData(){
    return this.http.get(this.localDataUrl).pipe(
      map(result=>{
        
        return <LocalDataSummary[]>Object.values(result);

      })


    )
  }
}
        
         

  
