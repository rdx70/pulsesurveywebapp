import { config } from '../app/config';

export const environment = {
  production: true,
  hostUrl: config.appUrl,
  apiURl: config.apiUrl,
  version:'0.0.1'
};
